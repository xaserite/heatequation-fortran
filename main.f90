program HEATEQ_test
    use module_HEATEQ
    use riemann_problem_double
    implicit none
    type(RP) :: P
    type(HEATEQ) :: c
    character(len=*),parameter :: FILE_IN='problem.data', FILE_OUT_RO='HEATEQ.data', FILE_PARAM='../param.data'
    integer :: x_N,t_N
    real(8) :: T,I_l=-1.,I_r=1.,ro_l=0.,ro_m=1.,ro_r=0., l1,l2
	! read parameters
	open(unit=99,file=FILE_PARAM)
		read(99,*) x_N, t_N
		read(99,*) T
		read(99,*) l1,l2
	close(99)
	x_N = 500
	t_N = 200000
	! initialize
    call P%setup(x_N,t_N,T,I_l,I_r,ro_l,ro_m,ro_r,l1,l2)
    call P%toFile(FILE_IN)
    call c%fromFile(FILE_IN)
    call P%free
    call c%compute
    call c%roToGnuplot(FILE_OUT_RO)
    call c%free
end program HEATEQ_test
