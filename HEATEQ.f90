module module_HEATEQ
  implicit none
    external inv
  private
  type, public :: HEATEQ
    real(8),allocatable,dimension(:,:):: ro, A, Ainv
    real(8),allocatable,dimension(:)  :: x
    real(8) :: T, dx, dt, l1,l2
    integer :: x_N, t_N , it_t
  contains
    procedure :: init => HEATEQ_init
    procedure :: fromFile => HEATEQ_fromFile
    procedure :: compute => HEATEQ_compute
    procedure :: toFile => HEATEQ_toFile
    procedure :: roToGnuplot => HEATEQ_roToGnuplot
    procedure :: free => HEATEQ_free
  end type HEATEQ
contains
! class creators
subroutine HEATEQ_init(this,ro_0,x_0,T,t_N,l1,l2)
    class(HEATEQ) :: this
    real(8),dimension(:),intent(in) :: ro_0, x_0
    real(8),intent(in) :: T, l1,l2
    integer,intent(in) :: t_N
    this%x_N = size(x_0)
    this%x = x_0
    this%T = T
    this%t_N = t_N
    this%dt = T/t_N
    this%dx = x_0(2) - x_0(1)
    this%l1 = l1
    this%l2 = l2
    allocate (this%ro(this%x_N,this%t_N))
    this%ro(:,1) = ro_0
    !call HEATEQ_build_system(this)
end subroutine HEATEQ_init
! backward Euler system matrix A and its inverse Ainv
subroutine HEATEQ_build_system(this)
    class(HEATEQ) :: this
    integer :: k
    real(8) :: l1, l2 , l3
    allocate(this%A(this%x_N,this%x_N))
    allocate(this%Ainv(this%x_N,this%x_N))
    l1 = this%dt/this%dx/this%dx
    l2 = 1+2*l1
    l3 = 1+l1
    this%A(1,1) = l3
    this%A(1,2) = -l1
    this%A(this%x_N,this%x_N) = l3
    this%A(this%x_N,this%x_N-1) = -l1
    do k=2,this%x_N-1
        this%A(k,k) = l2
        this%A(k,k-1)=-l1
        this%A(k,k+1)=-l1
    end do
    this%Ainv = this%A
    call inv(this%A,this%Ainv)
end subroutine
!
! solver
subroutine HEATEQ_compute(this)
class(HEATEQ) :: this
    this%it_t = 1
    do while(this%it_t<this%t_N-1)
        call HEATEQ_forwardEuler(this)
        !call HEATEQ_backwardEuler(this)
        !call HEATEQ_emulation(this)
        this%it_t = this%it_t+1
    end do
end subroutine
!
!
subroutine HEATEQ_forwardEuler(this)
    class(HEATEQ) :: this
    real(8) :: lambda
    real(8),dimension(2) :: nbrs
    integer :: k
    lambda = this%dt/this%dx/this%dx
    do k = 1,this%x_N
        nbrs = HEATEQ_ro_nbrs(this,k)
        this%ro(k,this%it_t+1) = this%ro(k,this%it_t) &
        &+ lambda/this%l1 * (nbrs(1)-2*this%ro(k,this%it_t)+nbrs(2)) &
        &+ .5* this%l2/this%l1 *this%dt/this%dx * (nbrs(1)-nbrs(2))
    end do
end subroutine
!
!
subroutine HEATEQ_backwardEuler(this)
    class(HEATEQ) :: this
    this%ro(:,this%it_t+1) = matmul( this%Ainv , this%ro(:,this%it_t) )
end subroutine
!
!
subroutine HEATEQ_emulation(this)
    class(HEATEQ) :: this
    real(8) :: lambda, l2
    real(8),dimension(2) :: nbrs1, nbrs2
    integer :: k
    l2 = this%dt/this%dx
    lambda = l2/this%dx/4
    do k = 1,this%x_N
        nbrs1 = HEATEQ_ro_nbrs(this,k)
        nbrs2 = HEATEQ_ro_nbrs2(this,k)
        this%ro(k,this%it_t+1) = this%ro(k,this%it_t) &
        &+ lambda * (nbrs2(1)-2*this%ro(k,this%it_t)+nbrs2(2)) &
        &+ l2 * (nbrs1(2) -2*this%ro(k,this%it_t) + nbrs1(1))
    end do
end subroutine
!
! neighbour functions
function HEATEQ_ro_nbrs(this,i) result(nbrs)
class(HEATEQ) :: this
    integer :: i
    real(8),dimension(2) :: nbrs
    if(i==1) then
        nbrs(1) = this%ro(2,this%it_t)
    else
        nbrs(1) = this%ro(i-1,this%it_t)
    end if
    if(i==this%x_N) then
        nbrs(2) = this%ro(i-1,this%it_t)
    else
        nbrs(2) = this%ro(i+1,this%it_t)
    end if
end function
function HEATEQ_ro_nbrs2(this,i) result(nbrs)
class(HEATEQ) :: this
    integer :: i
    real(8),dimension(2) :: nbrs
    if(i==1 .or. i==2) then
        nbrs(1) = this%ro(1,this%it_t)
    else
        nbrs(1) = this%ro(i-2,this%it_t)
    end if
    if(i==this%x_N .or. i==this%t_N-1) then
        nbrs(2) = this%ro(i,this%it_t)
    else
        nbrs(2) = this%ro(i+2,this%it_t)
    end if
end function
!
! file IO functions
subroutine HEATEQ_toFile(this,FILENAME)
    class(HEATEQ) :: this
    character(len=*):: FILENAME
    integer :: k
    open(unit = 1, file = trim(FILENAME))
    write(1,*) this%x_N, this%t_N
    write(1,*) this%x
    do k = 1,this%t_N
        write(1,*) this%ro(:,k)
    end do
    close(1)
end subroutine
subroutine HEATEQ_roToGnuplot(this,FILENAME)
    class(HEATEQ) :: this
    character(len=*):: FILENAME
    integer :: k
    open(unit = 1, file = trim(FILENAME))
    do k=1,this%x_N
        write(1,*) this%x(k), this%ro(k, this%t_N-1)
    end do
end subroutine
subroutine HEATEQ_fromFile(this,FILENAME)
    class(HEATEQ) :: this
    character(len=*):: FILENAME
    real(8),dimension(:),allocatable :: ro_0, x_0
    real(8) :: T, l1,l2
    integer :: x_N, t_N
    open(unit = 3, file = FILENAME, status='old')
    read(3,*) x_N, t_N, T
    allocate(x_0(x_N))
    allocate(ro_0(x_N))
    read(3,*) x_0
    read(3,*) ro_0
    read(3,*) l1,l2
    close(3)
    call HEATEQ_init(this,ro_0,x_0,T,t_N,l1,l2)
    deallocate(x_0)
    deallocate(ro_0)
end subroutine
!
! free routine
subroutine HEATEQ_free(this)
    class(HEATEQ) :: this
    if(allocated(this%x))then
        deallocate(this%x)
    end if
    if(allocated(this%ro))then
        deallocate(this%ro)
    end if
end subroutine
end module

