module riemann_problem_double
implicit none
private
    type, public :: RP
        real(8),dimension(:),allocatable :: x, ro, j
            real(8) :: T, l1,l2
            integer :: x_N, t_N
        contains
            procedure :: setup => RP_setup
            procedure :: free => RP_free
            procedure :: toFile => RP_toFile
    end type RP
contains
	subroutine RP_setup(this,x_N,t_N,T,I_l,I_r,ro_l,ro_m,ro_r,l1,l2)
		class(RP) :: this
		real(8),intent(in) :: T, I_l,I_r,ro_l,ro_m,ro_r, l1,l2
		integer, intent(in) :: x_N, t_N
		real(8) :: length, dx
		integer :: x_midNl,x_midNr, k
		length = I_r - I_l
		dx = length/(x_N-1)
		x_midNl = floor(.4*x_N)
		x_midNr = floor(.6*x_N)
		this%x_N = x_N
		this%t_N = t_N
		this%T = T
		this%l1 = l1
		this%l2 = l2
		allocate(this%x(x_N))
		allocate(this%ro(x_N))
		do k = 1,x_midNl
			this%x(k) = I_l + (k-1)*dx
			this%ro(k) = ro_l
		end do
		do k = x_midNl+1,x_midNr
			this%x(k) = I_l + (k-1)*dx
			this%ro(k) = ro_m
		end do
		do k = x_midNr+1,x_N
			this%x(k) = I_l + (k-1)*dx
			this%ro(k) = ro_r
		end do
	end subroutine
	subroutine RP_free(this)
		class(RP) :: this
		if(allocated(this%x))then
			deallocate(this%x)
		end if
		if(allocated(this%ro))then
			deallocate(this%ro)
		end if
	end subroutine
	subroutine RP_toFile(this,FILENAME)
		class(RP) :: this
		character(len=*) :: FILENAME
		open(unit=1,file=FILENAME)
			write(1,*) this%x_N, this%t_N, this%T
			write(1,*) this%x
			write(1,*) this%ro
			write(1,*) this%l1,this%l2
		close(unit=1)
	end subroutine
end module
